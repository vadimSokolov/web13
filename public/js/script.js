document.addEventListener('DOMContentLoaded', main);

function main() {
	document.getElementById('completeForm').addEventListener('click', showForm);
}

function showForm() {
	window.history.pushState(null, null, '?Form');

	getItemForm();

	document.querySelector('.popapp-body').classList.add('popapp-averlay');
	document.querySelector('.container').classList.add('popapp');
}
window.addEventListener('submit', (e) => {
	e.preventDefault();
	if(checkiInputs()) {
		
		getItemForm();

		console.log('1')

		const inputs = document.querySelectorAll('.form input');
	
		$.ajax({
			url: 'https://api.slapform.com/mamko_drobitel@mail.ru',
			dataType: "json",
			method: 'POST',
			data: {
				name: inputs[0].value,
				email: inputs[1].value,
				password: inputs[2].value,
			},
			success: function (response) {
				console.log("Response: ", response);
				if (response.meta.status == 'success') {
					closed();
					alert('Отправка данных успешна');
					localStorage.clear();
				} else if (response.meta.status == 'fail') {
					alert("Ошибка");
					console.log('Submission failed with these errors: ', response.meta.errors);
				}
			}
		});
		console.log('2')
	}
 
})

function checkiInputs() {
	const form = document.getElementById('form'),
		username = document.getElementById('username'),
		email = document.getElementById('email'),
		password = document.getElementById('password'),
		password2 = document.getElementById('password2');

	//get the value from the inputs
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();

	if (!isCorrect(usernameValue)) {
		//show error
		//add error class
		addErrorFor(username, 'Username cannot be blank');
		return false;
	} else {
		//add success class
		addSuccessFor(username);
	}

	if (!isCorrect(emailValue)) {
		addErrorFor(email, 'Email cannot be blank');
		return false;
	} else {
		addSuccessFor(email);
	}

	if (!isCorrect(passwordValue)) {
		addErrorFor(password, 'Password cannot be blank');
		return false;
	} else {
		addSuccessFor(password);
	}

	if (!isCorrect(password2Value)) {
		addErrorFor(password2, 'Password cannot be blank');
		return false;
	} else if (password2Value !== passwordValue) {
		addErrorFor(password2, 'Password does not match');
		return false;
	} else {
		addSuccessFor(password2);
	}

	if (isCorrect(usernameValue) && isCorrect(emailValue) &&
		isCorrect(passwordValue) && (password2Value === passwordValue)) {
			setItemForm();
			document.getElementById("form").submit();
			window.history.pushState(null, null, '? ');
	}
	console.log("kjdfdslfjlkds");
	return true;
}

function isCorrect(input) {
	if (input === '') return false;
	if (input.indexOf(' ') != -1) return false;

	return true;
}

function addErrorFor(input, message) {
	const formControl = input.parentElement,
		small = formControl.querySelector('small');

	//add error message insode small
	small.innerHTML = message;

	//add error class
	formControl.classList.remove('success')
	formControl.classList.add('error')
}

function addSuccessFor(input) {
	const formControl = input.parentElement;

	//add success class 
	formControl.classList.remove('error')
	formControl.classList.add('success');
}

function closed() {
	setItemForm();

	window.history.pushState(null, null, '? ');
	document.querySelector('.popapp-body').classList.remove('popapp-averlay');
	document.querySelector('.container').classList.remove('popapp');
}

function setItemForm() {
	const inputs = document.querySelectorAll('.form input');

	localStorage.setItem('username', inputs[0].value);
	localStorage.setItem('email', inputs[1].value);
	localStorage.setItem('password', inputs[2].value);
	localStorage.setItem('password2', inputs[3].value);

}

function getItemForm() {
	const inputs = document.querySelectorAll('.form input');

	inputs[0].value = localStorage.getItem('username');
	inputs[1].value = localStorage.getItem('email');
	inputs[2].value = localStorage.getItem('password');
	inputs[3].value = localStorage.getItem('password2');

	for(let i = 0; i < inputs.length; i++) {
		console.log(inputs[i].value);	
	}

}

